﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebUI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //  url / - выводит первую страницу списка товаров всех категорий 

            routes.MapRoute(
                null,
                "",
                new { controller = "Books", action = "List", genre = (string)null, page = 1 }
            );


            //  url /PageX - выводит указанную страницу, отображая товары всех категорий
            routes.MapRoute(
                name: null,
                url: "Page{page}",
                defaults: new { controller = "Books", action = "List", genre = (string)null },
                constraints: new { page = @"\d+" }
            );

            //  url /Программирование  - отображает первую страницу элементов указанной категории
            routes.MapRoute(
                null,
                "{genre}",
                new { controller = "Books", action = "List", page = 1 }
            );

            //  url /Программирование/PageX - отображает заданную страницу элементов указанной категории
            routes.MapRoute(
                null,
                "{genre}/Page{page}",
                new { controller = "Books", action = "List" },
                new { page = @"\d+" }
            );

            routes.MapRoute(
                null,
                "{controller}/{action}"
            );
        }
    }
}
