﻿using Domain.Abstract;
using Domain.Concrete;
using Domain.Entities;
using Moq;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebUI.Infrastructure
{
    public class NinjectDependencyResolver: IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }

        private void AddBindings()
        {
            //Mock<IBookRepository> mock = new Mock<IBookRepository>();
            //mock.Setup(m => m.Books).Returns(new List<Book>
            //{
            //    new Book{Name = "Язык программирования C# 5.0 и платформа. NET 4.5'", Author = "Троелсен Э.", Price = 1776},
            //    new Book{Name = "Красное и черное", Author = "Стендаль Ф.", Price = 194},
            //    new Book{Name = "Разговорный английский для тех, кто много путешествует + 2 CD", Author = "Черниховская Н.О.", Price = 472}
            //});
            //kernel.Bind<IBookRepository>().ToConstant(mock.Object);
            
            kernel.Bind<IBookRepository>().To<EFBookRepository>();
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        } 
    }
}